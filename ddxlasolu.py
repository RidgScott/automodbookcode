"""This demo program solves Poisson's equation

    - div grad u(x, y) = f

on the unit square with Neumann boundary condition

        du/dn = g on { x = 1 },  g(y)=1

on part of the boundary of the square, whose solution is 
unknown, but there is a mismatch at (0,1) and (1,1)

"""
from dolfin import *
import sys,math

meshsize=int(sys.argv[1])

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", 1)

#Dirichlet boundary (x=0 or y=0 or y=1)
def boundary(x):
  return x[0] < DOLFIN_EPS or x[1] < DOLFIN_EPS or x[1] > 1 - DOLFIN_EPS

# Define boundary condition
u0 = Expression("0.0",degree=1)
bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("0.0",degree=1)
g = Expression("1.0",degree=1)
a = inner(grad(u), grad(v))*dx 
L = f*v*dx + g*v*ds

# Compute solution
u = Function(V)
solve(a == L, u, bc)
v=grad(u)[0]
plot(v, interactive=True)