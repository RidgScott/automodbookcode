"""This program solves the Navier-Stokes equations
on the unit square with Dirichlet boundary conditions given by 
u=(1,0) on the top and u = 0 elsewhere, the driven cavity.
"""
from dolfin import *
import math,sys

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
fudg=float(sys.argv[3])
reno=float(sys.argv[4])

# create mesh and define velocity and pressure function spaces 
mesh = UnitSquareMesh(meshsize, meshsize, "crossed")
newmesh = refine(mesh)
newmesh = refine(newmesh)
V = VectorFunctionSpace(mesh, "Lagrange", pdeg)
W = VectorFunctionSpace(newmesh, "Lagrange", 1)
# define boundary condition
boundary_exp = Expression(("exp(-fu*(1.0-x[1])*(1.0-x[1]))","0"),fu=fudg)
bc = DirichletBC(V, boundary_exp, "on_boundary")
# set the parameters
f = Expression(("0","0"))
bcz = DirichletBC(V, f, "on_boundary")
r = 1.0e3
# define test and trial functions, and function that is updated
uold = TrialFunction(V)
v = TestFunction(V)
w = Function(V)
asf = inner(grad(uold), grad(v))*dx + r*div(uold)*div(v)*dx 
bs = -div(w)*div(v)*dx
uold = Function(V)
ust = Function(V)
pdes = LinearVariationalProblem(asf, - bs, uold, bc)
solvers = LinearVariationalSolver(pdes)
# Scott-Vogelius iterated penalty method
iters = 0; max_iters = 15; div_u_norm = 1
while iters < max_iters and div_u_norm > 1e-10:
# solve and update w
    solvers.solve()
    w.vector().axpy(-r, uold.vector())
# find the L^2 norm of div(u) to check stopping condition
    div_u_norm = sqrt(assemble(div(uold)*div(uold)*dx(mesh)))
#       print "   IPM iter_no=",iters,"div_u_norm="," %.2e"%div_u_norm
    iters += 1
print "Stokes solver  IPM iter_no=",iters,"div_u_norm="," %.2e"%div_u_norm
ust.vector().axpy(1.0, uold.vector())

# In the book, the code breaks here. The top part is Program 21.1, page 234
# The bottom part that follows is Program 21.2, page 235

# set the variational problem
kters = 0; max_kters = 9; unorm = 1
while kters < max_kters and unorm > 1e-9:
    u = TrialFunction(V)
    v = TestFunction(V)
    w = Function(V)
#   uold = Function(V)
    a = inner(grad(u), grad(v))*dx + r*div(u)*div(v)*dx \
      +reno*inner(grad(uold)*u,v)*dx+reno*inner(grad(u)*uold,v)*dx 
    b = -div(w)*div(v)*dx
    F = inner(grad(uold), grad(v))*dx+reno*inner(grad(uold)*uold,v)*dx
    u = Function(V)
    pde = LinearVariationalProblem(a, F - b, u, bcz)
    solver = LinearVariationalSolver(pde)

# Scott-Vogelius iterated penalty method
    iters = 0; max_iters = 11; div_u_norm = 1
    while iters < max_iters and div_u_norm > 1e-10:
    # solve and update w
        solver.solve()
        w.vector().axpy(-r, u.vector())
    # find the L^2 norm of div(u) to check stopping condition
        div_u_norm = sqrt(assemble(div(u)*div(u)*dx(mesh)))
#       print "   IPM iter_no=",iters,"div_u_norm="," %.2e"%div_u_norm
        iters += 1
    print "   IPM iter_no=",iters,"div_u_norm="," %.2e"%div_u_norm
    kters += 1
    uold.vector().axpy(-1.0, u.vector())
#   uold=uold-u
    unorm=norm(u,norm_type='H1')
    uoldnorm=norm(uold,norm_type='H1')
    print "Newton iter_no=",kters,"Delta_u_norm="," %.2e"%unorm,"u_norm="," %.2e"%uoldnorm
uold.vector().axpy(-1.0, ust.vector())
plot(uold[0], interactive=True)

# Ultimate Stokes Algorithm (USA)
p_US = project(div(w), FunctionSpace(mesh, "Lagrange", pdeg - 1))
