from dolfin import *
import sys,math
from timeit import default_timer as timer

startime=timer()
meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define Dirichlet boundary (x = 0 or x = 1 or y = 0 or y = 1)
def boundary(x):
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS or x[1] < DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("(sin(mypi*x[0]))*(sin(mypi*x[1]))",mypi=math.pi)
a = inner(grad(u), grad(v))*dx
L = (2*math.pi*math.pi)*f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u, bc)
aftersolveT=timer()
totime=aftersolveT-startime
print " ",pdeg," ",meshsize," %.2e"%errornorm(f,u,norm_type='l2', degree_rise=3)," %.3f"%totime