"""This demo program solves the BBM equation
on the interval [-L,L] with initial data g given by

    g(x) = e^{-x^2}

and boundary conditions given by

    u(-L) = 0
    u(L) = 0

solved by modified Euler.
"""
import sys, math
from dolfin import *
import time

dt=0.025
enn=1200
#enn=1000
deg=1
mno=5000
ell=90
ellmin=-30

# Create mesh and define function space
mesh = IntervalMesh(mno,ellmin,ell)
V = FunctionSpace(mesh, "Lagrange", deg)

# Define initial and boundary conditions
g = Expression("exp(-(pow(x[0], 2)))")
u0 = Expression("0")

bc = DirichletBC(V, u0, "on_boundary")

# Define variational problem
u = TrialFunction(V)
uold = Function(V)
unew = Function(V)
v = TestFunction(V)
a = inner(grad(u), grad(v))*dx + u*v*dx
FP = inner(grad(uold), grad(v))*dx + uold*v*dx \
    - dt*((1+2*uold)*uold.dx(0)*v)*dx
FC = inner(grad(uold), grad(v))*dx + uold*v*dx \
    - dt*((1+2*uold)*uold.dx(0)*v)*dx \
    - dt*((1+2*unew)*unew.dx(0)*v)*dx
u = Function(V)

T = enn*dt
t = 0

uold.interpolate(g)
u.assign(uold)
plot(u,window_width=2000,range_min=-0.5,range_max=0.5)
#plot(u,window_width=2000,range_min=-0.5,range_max=1.0)
time.sleep(3)

while t <= T:
    # Compute solution
    solve(a == FP, unew, bc)
    solve(a == FC, u, bc)
    uold.assign(u)
    plot(u)
#   plot(u,rescale=False)
    time.sleep(.02)
    t += dt
interactive()