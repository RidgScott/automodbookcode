"""This demo program solves Poisson's equation

    - div grad u(x, y) = pi*pi*sin(pi*x)*sin(pi*y)

on the unit square with Robin boundary conditions given by 

        alfa u + du/dn = 0 

on the boundary of the square, whose solution is not known
explicity.

"""
from dolfin import *

# Create mesh and define function space
mesh = UnitSquareMesh(32, 32)
V = FunctionSpace(mesh, "Lagrange", 1)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("(sin(3.141592*x[0]))*(sin(3.141592*x[1]))",degree=1)
alfa = 1.0
a = inner(grad(u), grad(v))*dx + alfa*u*v*ds
L = (2*3.141592*3.141592)*f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u)

# Plot solution
plot(u, interactive=True)