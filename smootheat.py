"""This demo program solves the heat equation

    u_t - u''(x, y) = f(x, y)

on the unit interval with initial data g given by

    g(x) = |x-1/2|

and boundary conditions given by

    u(0) = 0
    u(1) = 1
"""
import sys, math
from dolfin import *
import time

dt=float(sys.argv[1])
deg=int(sys.argv[2])
mno=int(sys.argv[3])

# Create mesh and define function space
mesh = UnitIntervalMesh(mno)
V = FunctionSpace(mesh, "Lagrange", deg)

# Define Dirichlet boundary (x = 0 or x = 1)
def boundary(x):
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

# Define boundary condition
g = Expression("0.5-std::abs(x[0]-0.5)",degree=deg)
u0 = Expression("0",degree=deg)

bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = TrialFunction(V)
uold = Function(V)
v = TestFunction(V)
a = dt*inner(grad(u), grad(v))*dx + u*v*dx
F = uold*v*dx
u = Function(V)

uold.interpolate(g)
u.assign(uold)

# Compute one time step
solve(a == F, u, bc)

uold.assign(u)
plot(u, interactive=True)