"""This program solves a PDE

    - div grad u(x, y) = 1

on the unit square with Dirichlet boundary conditions given by u=0 on
(x,0) for x=[1/2,1] which has a signularity at (1/2,0) of the form

       ((x-1/2)^2+y^2)^{1/2}\sin\theta.

"""

from dolfin import *
import sys,math

#parameters["form_compiler"]["quadrature_degree"] = 12

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

#Dirichlet boundary (x = 0 or x = 1 or y = 0 or y = 1)
def boundary(x):
  return x[0] > 0.5-DOLFIN_EPS and x[1] < DOLFIN_EPS 

# Define boundary condition
u0 = Expression("0.0",degree=pdeg)
bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("1.0",degree=pdeg)
a = (inner(grad(u), grad(v)))*dx
L = f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u, bc)
#print " ",pdeg," ",qdeg," ",meshsize," %.2e"%myeps," %.1f"%ell," %.3f"%totime,mfu,myr
# Plot solution
plot(u, interactive=True)