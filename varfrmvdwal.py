"""This demo program solves a PDE

    - div grad u(x, y) + K(x,y) u(x,y) = f

on the unit square with Dirichlet boundary conditions given by u=0 
on the boundary of the square, whose solution is related to the
asymptotic van der Waals interaction energy for two hydrogen atoms.
Computation is truncated to a domain of length "ell"
"""

from dolfin import *
import sys,math
from timeit import default_timer as timer

parameters["form_compiler"]["quadrature_degree"] = 12

startime=timer()
meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
ell=float(sys.argv[3])
myeps=float(sys.argv[4])

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, DomainBoundary())

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("-(2.0*pow(el,6)/mypi)*pow(x[0]*x[1],2)*exp(-el*x[0]-el*x[1])",el=ell,mypi=math.pi)
kay = Expression("(2.0/(me+pow(x[0],2)))-2.0*(el/(me+x[0]))+(2.0/(me+pow(x[1],2)))-2.0*(el/(me+x[1]))+2.0*el*el",me=myeps,el=ell)
a = (inner(grad(u), grad(v))+kay*u*v)*dx
m = u*v*dx
RHS = f*v*dx 

# Compute solution
u = Function(V)
solve(a == RHS, u, bc)
aftersolveT=timer()
mfu= (16.0*pow(math.pi,2)/3.0)*assemble(u*f*dx)
mer=mfu-6.49902670540
totime=aftersolveT-startime
print " ",pdeg," ",meshsize," %.2e"%mer," %.2e"%myeps," %.1f"%ell," %.3f"%totime