"""This demo program solves Poisson's equation

    - div grad u(x, y) = 2x

on the unit square with Neumann boundary condition

        du/dn = g on { x = 1 },  g(y)=y(1-y)

on part of the boundary of the square, whose solution is 
u(x,y)=y(1-y)x

"""
from dolfin import *
import sys

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

#Dirichlet boundary (x=0 or y=0 or y=1)
def boundary(x):
  return x[0] < DOLFIN_EPS or x[1] < DOLFIN_EPS or x[1] > 1 - DOLFIN_EPS

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, boundary)


# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("2.0*x[0]",degree=pdeg)
g = Expression("x[1]*(1-x[1])",degree=pdeg)
ue = Expression("x[0]*x[1]*(1-x[1])",degree=pdeg)
a = inner(grad(u), grad(v))*dx 
L = f*v*dx + g*v*ds

# Compute solution
u = Function(V)
solve(a == L, u, bc)

uze=interpolate(ue,V)

print " ",pdeg," ",meshsize," %.2e"%errornorm(ue,u,norm_type='l2', degree_rise=3)

# Plot solution
plot(u, interactive=True)
#plot(u-uze, interactive=True)