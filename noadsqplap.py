"""This program solves the p_Laplacian

    - div |\nabla u|^{p-2} grad u(x, y) = 1

on the unit square with homogeneous Dirichlet boundary conditions
"""

from dolfin import *
import sys,math

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
pee=float(sys.argv[3])
ep=float(sys.argv[4])
eps=ep*ep
pmt=(pee-2.0)/2.0

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define boundary condition
u0 = Expression("0.0",degree=pdeg)
bc = DirichletBC(V, u0, "on_boundary")

# Define variational problem
u = Function(V)
v = TestFunction(V)
f = Expression("1.0",degree=pdeg)
F = pow(eps+(inner(grad(u), grad(u))),pmt)*(inner(grad(u), grad(v)))*dx - f*v*dx

# Compute solution
solve(F == 0, u, bc)

maxu=norm(u,norm_type='l2')
print "mesh","pdeg","    pmt","    eps","      p=","    L2norm u="
print meshsize," ",pdeg," "," %.2e"%pmt," %.2e"%ep," %.2f"%pee," %.2e"%maxu

# Plot solution
plot(u.leaf_node(), interactive=True)
