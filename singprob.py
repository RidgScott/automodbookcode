"""This demo program solves Poisson's equation

    - div grad u(x, y) = f(x,y)

where f(x,y)=2*pi^2*cos(pi*x)*cos(pi*y)
on the unit square with pure Neumann boundary conditions 
on the boundary of the square, whose solution is

    u(x, y) = cos(pi*x)*cos(pi*y)

"""

from dolfin import *
import sys,math

meshsize=int(sys.argv[1])
degree=int(sys.argv[2])

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", degree)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("(cos(mypi*x[0]))*(cos(mypi*x[1]))",mypi=math.pi)
a = inner(grad(u), grad(v))*dx
L = (2*(math.pi)*(math.pi))*f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u)
meanu=assemble(u*dx)
mu = Expression("meanyou",meanyou=meanu)
mconstu = Function(V)
mconstu=interpolate(mu,V)
fo=interpolate(f,V)
um = Function(V)
um.assign(u - mconstu)
inteff=assemble(fo*dx)
print meshsize,degree," %.1e"%meanu," %.1e"%inteff," %.1e"%(meanu/inteff)," %.2e"%errornorm(f,um,norm_type='l2', degree_rise=3)