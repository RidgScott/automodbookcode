"""This program solves a PDE

    - div alfa(x,y) grad u(x, y) = -6

on the unit square with Dirichlet b undary conditions given by 
u=0 on (0,y) for y=[0,1], and u=3/2 on (1,y) for y=[0,1] 
with a coefficient alfa that has a near discontinuity at x=0.5

alfa(x,y)= 2+tanh(fudg*(x-0.5)) --> 1 for x<1/2 and 3 for x>1/2

The exact solution (for fudg=\infty) is

u(x,y)= 3x^2 for x<1/2
u(x,y)= 0.5 + x^2 for x>1/2

"""

from dolfin import *
import sys,math

#parameters["form_compiler"]["quadrature_degree"] = 12

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
fudg=int(sys.argv[3])

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

#Dirichlet boundary (x = 0 or x = 1)
def boundary(x):
  return x[0] < DOLFIN_EPS or x[0] > 1- DOLFIN_EPS 

# Define boundary condition
u0 = Expression("1.5*x[0]")
bc = DirichletBC(V, u0, boundary)

ul = Expression("3.0*x[0]*x[0]")
ur = Expression("0.5+x[0]*x[0]")

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("-6.0")
alfa = Expression("2.0+tanh(fud*(x[0]-0.5))",fud=fudg)
a = (alfa*(inner(grad(u), grad(v))))*dx
L = f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u, bc)

# Plot solution
plot(u, interactive=True)