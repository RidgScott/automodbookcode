"""This program solves a PDE

    - div grad u(x, y) = 1

on the unit square with Dirichlet boundary conditions given by u=0 on
(x,0) for x=[1/2,1] which has a signularity at (1/2,0) of the form

       ((x-1/2)^2+y^2)^{1/2}\sin\theta.

using an adaptive mesh
"""

from dolfin import *
import sys,math

parameters["form_compiler"]["quadrature_degree"] = 12

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
qdeg=int(sys.argv[3])
mytol=float(sys.argv[4])

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define Dirichlet boundary (x = 0 or x = 1 or y = 0 or y = 1)
def boundary(x):
  return x[0] > 0.5 and x[1] < DOLFIN_EPS 

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = Function(V)
v = TestFunction(V)
f = Expression("1.0")
J = u*u*dx 
F = (inner(grad(u), grad(v)))*dx - f*v*dx

# Compute solution
solve(F == 0, u, bc, tol=mytol, M=J)

# Plot solution
plot(u.leaf_node(), interactive=True)