from dolfin import *

# create mesh and define velocity and pressure function spaces 
meshsize = 16
mesh = UnitSquareMesh(meshsize, meshsize, "crossed")
k=4
V = VectorFunctionSpace(mesh, "Lagrange", k)

# define boundary condition
gee = Expression(("sin(4*pi*x[0])*cos(4*pi*x[1])",
                 "-cos(4*pi*x[0])*sin(4*pi*x[1])"),degree=k)
bc = DirichletBC(V, gee, "on_boundary")

# set the parameters
f = Expression(("28*pow(pi, 2)*sin(4*pi*x[0])*cos(4*pi*x[1])",
               "-36*pow(pi, 2)*cos(4*pi*x[0])*sin(4*pi*x[1])"),degree=k)
r = 1.0e3
# define test and trial functions, and function that is updated
u = TrialFunction(V)
v = TestFunction(V)
w = Function(V)

# set the variational problem
a = inner(grad(u), grad(v))*dx + r*div(u)*div(v)*dx
b = -div(w)*div(v)*dx
F = inner(f, v)*dx
u = Function(V)
pde = LinearVariationalProblem(a, F - b, u, bc)
solver = LinearVariationalSolver(pde)

# Scott-Vogelius iterated penalty method
iters = 0; max_iters = 100; div_u_norm = 1
while iters < max_iters and div_u_norm > 1e-10:
    # solve and update w
    solver.solve()
    w.vector().axpy(-r, u.vector())
    # find the L^2 norm of div(u) to check stopping condition
    div_u_norm = sqrt(assemble(div(u)*div(u)*dx(mesh)))
    print "norm(div u)=%.2e"%div_u_norm
    iters += 1
print k,meshsize," %.2e"%errornorm(gee,u,norm_type='l2', degree_rise=3)