"""This demo program solves the wave equation

    u_ty - u''(x, y) = 0

on the interval [-L,L] with initial data g given by

    g(x) = e^{-x^2}

and boundary conditions given by

    u(-L) = 0
    u(L) = 0

solved by the theta method with theta=1/4
"""
import sys, math
from dolfin import *
import time

dt=0.05
enn=400
deg=1
mno=2000
ell=10

tau=0.25*dt*dt

# Create mesh and define function space
mesh = IntervalMesh(mno,-ell,ell)
V = FunctionSpace(mesh, "Lagrange", deg)

# Define boundary condition
g = Expression("exp(-(pow(x[0], 2)))")
u0 = Expression("0")

bc = DirichletBC(V, u0, "on_boundary")

# Define variational problem
u = TrialFunction(V)
uold = Function(V)
uolder = Function(V)
v = TestFunction(V)
a = tau*inner(grad(u), grad(v))*dx + u*v*dx
F = 2.0*uold*v*dx - uolder*v*dx - 2.0*tau*inner(grad(uold), grad(v))*dx \
    - tau*inner(grad(uolder), grad(v))*dx
u = Function(V)

T = enn*dt
t = 0

uold.interpolate(g)
uolder.interpolate(g)
u.assign(uold)
plot(u)
time.sleep(3)

while t <= T:
    # Compute solution
    solve(a == F, u, bc)
    uolder.assign(uold)
    uold.assign(u)
    ue = Expression("0.5*exp(-(pow(x[0]-tee, 2))) \
                   + 0.5*exp(-(pow(x[0]+tee, 2)))",tee=t)
    print " %.2f"%t," %.2e"%errornorm(ue,u,norm_type='l2', degree_rise=3)
    plot(u)
    time.sleep(.02)
    t += dt
interactive()
