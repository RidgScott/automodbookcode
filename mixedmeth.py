"""This program solves a PDE

    - div grad p(x, y) = 2*\pi^2 * (\cos \pi*x)*(\cos \pi*y)

on the unit square with boundary conditions \nn\cdot\nabla p = 0 on
{y=0} and {y=1} and p=g on {x=0} and {x=1} where
g(x,y)=(1-2*x)*(\cos \pi*y)

which has the exact solution p(x,y)=(\cos \pi*x)*(\cos \pi*y)
"""

from dolfin import *
import sys,math

meshsize=int(sys.argv[1])

# Create mesh
mesh = UnitSquareMesh(meshsize, meshsize)
n = FacetNormal(mesh)

# Define function spaces and mixed (product) space
BDM = FunctionSpace(mesh, "BDM", 1)
DG = FunctionSpace(mesh, "DG", 0)
W = BDM * DG

# Define trial and test functions
(uu, p) = TrialFunctions(W)
(vv, q) = TestFunctions(W)

# Define source function
f = Expression("2.0*mypi*mypi*(cos(mypi*x[0]))*(cos(mypi*x[1]))",mypi=math.pi)
pe = Expression("(cos(mypi*x[0]))*(cos(mypi*x[1]))",mypi=math.pi)
glc = Expression("(1.0-2.0*x[0])*(cos(mypi*x[1]))",mypi=math.pi)

G = Expression(("0.0","0.0"))

# Define variational form
a = (dot(uu, vv) + div(vv)*p + div(uu)*q)*dx
L = - f*q*dx + glc*inner(vv,n)*ds

# Define essential boundary
def boundary(x):
    return x[1] < DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS

bc = DirichletBC(W.sub(0), G, boundary)

# Compute solution
w = Function(W)
solve(a == L, w, bc)
(uu, p) = w.split()

print "mesh size = ",meshsize,"  L2 error = %.2e"%errornorm(pe,p,norm_type='l2')

# Plot uu and p
#plot(uu)
plot(p)
interactive()
