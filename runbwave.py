"""This program solves the viscous Burgers' equation

    u_t - eps*u'' + u u'= 0

on the unit interval with initial data g given by

    g(x) = exp^(-(x-0.5)^2*kluge)

and boundary conditions given by

    u(0) = 0
    u(1) = 0
"""
import sys, math
from dolfin import *

dt=.00005
kluge=100
deg=4
mno=1000
enn=10000
eps=0.0001

# Create mesh and define function space
mesh = UnitIntervalMesh(mno)
V = FunctionSpace(mesh, "Lagrange", deg)

# Define Dirichlet boundary (x = 0 or x = 1)
def boundary(x):
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

# Define boundary condition
u0 = Expression("exp(-(x[0]-0.5)*(x[0]-0.5)*kl)",kl=kluge)

bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = TrialFunction(V)
uold = Function(V)
v = TestFunction(V)
a = dt*eps*inner(grad(u), grad(v))*dx + dt*u*uold.dx(0)*v*dx + u*v*dx
F = uold*v*dx
u = Function(V)
T = enn*dt
t = 0

uold.interpolate(u0)
u.assign(uold)
plot(u)
time.sleep(1)

while t <= T:
    # Compute solution
    solve(a == F, u, bc)
    uold.assign(u)
    plot(u)
    print t
    t += dt
interactive()