"""This demo program solves Poisson's equation

    - div grad u(x, y) = pi*pi*sin(pi*x)*sin(pi*y)

on the unit square with Dirichlet boundary conditions given by u=0 
on the boundary of the square, whose solution is

    u(x, y) = sin(pi*x)*sin(pi*y)

"""

from dolfin import *

# Create mesh and define function space
mesh = UnitSquareMesh(32, 32)
V = FunctionSpace(mesh, "Lagrange", 1)

# Define Dirichlet boundary (x = 0 or x = 1 or y = 0 or y = 1)
#def boundary(x):
#    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS or x[1] < DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, DomainBoundary())
#bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
you = Expression("(sin(3.141592*x[0]))*(sin(3.141592*x[1]))",degree=1)
a = inner(grad(u), grad(v))*dx
L = (2*3.141592*3.141592)*you*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u, bc)

# Save solution in VTK format
file = File("poisson.pvd")
file << u

# Plot solution
plot(u, interactive=True, wireframe=True)