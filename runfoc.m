%  First-Order  Conservative (up-stream) differencing in space
b=[ +1 -1 ];
a=[ 1 ];
bigx=10000;
dx=1/bigx
numeps=0.5*dx
r=dx*(1:bigx);
cfl=0.1
% results depend only on cfl
dt=cfl*dx
yo=exp(-(10*(r-0.5)).^2);
nts=25000;
bigt=4*nts*dt
yu=yo;
npics=2;
for k=1:nts
    yu=yu-cfl*filter(b,a,0.5*(yu .* yu));
end
ymid=yu;
for k=1:nts
    yu=yu-cfl*filter(b,a,0.5*(yu .* yu));
end
plot(r,yo,"linewidth",3,r,ymid+bigt,"linewidth",3,r,yu+2*bigt,"linewidth",3)
title('upwind scheme')
xlabel('x')
ylabel('pseudo   time')
