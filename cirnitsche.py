"""This program solves Poisson's equation

    - Delta u(x, y) = 4

on a unit circle with Dirichlet boundary conditions given by u=0 
on the boundary of the square, whose solution is

    u(x, y) = 1-x^2-y^2

"""

from mshr import *
from dolfin import *
import sys, math

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
gamma=float(sys.argv[3])
segmnts=int(sys.argv[4])
h=1.0/float(meshsize)

# Create mesh and define function space
domain =  Circle(dolfin.Point(0.0, 0.0), 1.0)
#domain =  Circle(dolfin.Point(0.0, 0.0), 1.0,segmnts)

# Create mesh and define function space
mesh   = generate_mesh(domain, meshsize)

n = FacetNormal(mesh)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, "on_boundary")

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
ue = Expression("1.0-x[0]*x[0]-x[1]*x[1]")
f = Expression("4.0")
a = inner(grad(u), grad(v))*dx -u*(inner(n,grad(v)))*ds-v*(inner(n,grad(u)))*ds+(gamma/h)*u*v*ds
L = f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u )

print "poly degree=",pdeg,"meshsize=",meshsize,"gamma=","%.2e"%gamma,"error","%.2e"%errornorm(ue,u,norm_type='l2', degree_rise=3)

# Plot solution
plot(u, interactive=True)

uei=interpolate(ue,V)
#plot(uei-u, interactive=True)

