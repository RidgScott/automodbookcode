# README #

These codes are the ones used in Introduction to Automated Modeling with FEniCS by L. Ridgway Scott

Code 2.1, Page 15: sinsqfish.py

Code 2.3, Page 19: inhomogeneuman.py 

Code 4.1, Page 36: laplacertime.py 

Code 5.1, Page 45: robinbc.py

Code 6.1, Page 51: lapslip.py

Code 6.2, Page 54: deltagreen.py

Code 6.3, Page 56: ddxlasolu.py

Code 7.1, Page 69: varfrmvdwal.py 

Code 9.1, Page 94: jeffham.py

Code 9.2, Page 95: noadsqplap.py

Code 10.1, Page 110: smootheat.py 

Code 12.1, Page 131: waveqmovie.py 

Code 12.2, Page 132: bbmodeulr.py 

Code 14.1, Page 156: scotvogel.py

Code 15.1, Page 166: firstadvect.py 

Code 15.2, Page 167: transport.py 

Code 16.1, Page 180: stikslipadapt.py 

Code 16.2, Page 181: adapdrylaplot.py 

Code 17.1, Page 191: discont.py

Code 18.1, Page 202: mixedmeth.py 

Codes 21.1 and 21.2, Pages 234-5: nsdrivecav.py 

Code varconaston.py used to produce Figures 21.2-3

Code 22.1, Page 239: bignitsche.py 

Code 22.2, Page 244: cirnitsche.py 

Code 23.1, Page 254: runfoc.m

Code 23.2, Page 261: runbwave.py 

Code 25.1, Page 271: singprob.py