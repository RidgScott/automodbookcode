"""This program solves Jeffrey-Hamel problem

    - u'' + 4u + 6u^2 = C

on the unit interval with boundary conditions given by

    u(0) = 0
du/dn(1) = 0
"""

from dolfin import *
import sys,math

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
see=float(sys.argv[3])

# Create mesh and define function space
mesh = UnitIntervalMesh(meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define Dirichlet boundary (x = 0 or x = 1)
def boundary(x):
    return x[0] < DOLFIN_EPS 

# Define boundary condition
u0 = Expression("0.0",degree=pdeg)
bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = Function(V)
v = TestFunction(V)
f = Expression("C",C=see,degree=pdeg)
F = inner(grad(u), grad(v))*dx + 4.0*u*v*dx + 6.0*u*u*v*dx - f*v*dx

# Compute solution
solve(F == 0, u, bc)

# Save solution in VTK format
file = File("poisson.pvd")
file << u

# Plot solution
plot(u, interactive=True)