"""This program solves Poisson's equation

    - div grad u(x, y) = pi*pi*sin(pi*x)*sin(pi*y)

on the unit square with Dirichlet boundary conditions given by u=0 
on the boundary of the square, whose solution is

    u(x, y) = sin(pi*x)*sin(pi*y)

"""

from dolfin import *
import sys, math

mypi=math.pi

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
gamma=float(sys.argv[3])
h=1.0/float(meshsize)

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
n = FacetNormal(mesh)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("(sin(mypi*x[0]))*(sin(mypi*x[1]))",mypi=math.pi)
a = inner(grad(u), grad(v))*dx -u*(inner(n,grad(v)))*ds-v*(inner(n,grad(u)))*ds+(gamma/h)*u*v*ds
L = (2*mypi*mypi)*f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u )

print pdeg,meshsize," %.2e"%gamma," %.2e"%errornorm(f,u,norm_type='l2', degree_rise=3)