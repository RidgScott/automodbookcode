"""This program solves the transport equation

 acoef *  u(x, y) + (1,0)\cdot\nabla u(x,y) = f

on the unit square with Dirichlet boundary conditions given 
by u=g on the in-flow portion of the boundary of the square.
"""

from dolfin import *
import sys,math
from timeit import default_timer as timer

startime=timer()
pdeg=int(sys.argv[1])
meshsize=int(sys.argv[2])
acoef=float(sys.argv[3])

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define Dirichlet boundary (x = 0)
def boundary(x):
  return x[0] < DOLFIN_EPS 

# Define boundary condition
gee = Expression("(x[0]+(x[1]*x[1]*(1.0-(2.0/3.0)*x[1])))*exp(-ac*x[0])",ac=acoef,degree=pdeg)
uex = Expression("(x[0]+(x[1]*x[1]*(1.0-(2.0/3.0)*x[1])))*exp(-ac*x[0])",ac=acoef,degree=pdeg)
bee = Constant((1.0,0.0))
bc = DirichletBC(V, gee, boundary)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("exp(-ac*x[0])",ac=acoef,degree=pdeg)
a = (acoef*u*v+inner(bee,grad(u))*v)*dx
L = f*v*dx 

# Compute solution
u = Function(V)
solve(a == L, u, bc)
aftersolveT=timer()
totime=aftersolveT-startime
uerr=errornorm(uex,u,norm_type='l2')
print "pdeg", "mshsiz"," acoef","   uerr","  totime"
print " ",pdeg," ",meshsize," %.1e"%acoef," %.1e"%uerr," %.3f"%totime

# Plot solution
plot(u, interactive=True)