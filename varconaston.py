"""This program solves Navier-Stokes equations
in a suddenly expanded channel with Dirichlet boundary conditions given by 
u=(1-y^2,0) on the left,  u = (wi*(1-(wi*y)^2),0) on the right and
u=0 elsewhere, where wi is 1 over the width of the expanded channel.
Uses continuation to increase Re.
"""
from mshr import *
from dolfin import *
import math,sys

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
fudg=float(sys.argv[3])
#rein=float(sys.argv[4])

width=5.0
left=-3.0
right=60.0
#right=80.0
#left=-10.0
#right=100.0

# create mesh and define velocity and pressure function spaces 
domain =  Polygon([dolfin.Point(left, -1.0),\
                   dolfin.Point(0.0, -1.0),\
                   dolfin.Point(0.0, -width),\
                   dolfin.Point(right, -width),\
                   dolfin.Point(right, +width),\
                   dolfin.Point(0.0, +width),\
                   dolfin.Point(0.0, +1.0),\
                   dolfin.Point(left, +1.0)])

# Create mesh and define function space
mesh   = generate_mesh(domain, meshsize)
newmesh = refine(mesh)
newmesh = refine(newmesh)
W = VectorFunctionSpace(newmesh, "Lagrange", 1)

V = VectorFunctionSpace(mesh, "Lagrange", pdeg)
# define boundary condition
boundary_exp = Expression(("exp(-fu*(le-x[0])*(le-x[0]))*(1-x[1]*x[1]) + \
    wi*exp(-fu*(ri-x[0])*(ri-x[0]))*(1-wi*wi*x[1]*x[1])","0"), \
    le=left,ri=right,fu=fudg,wi=1/width)
bc = DirichletBC(V, boundary_exp, "on_boundary")
# set the parameters
f = Expression(("0","0"))
g = Expression(("0","0.000000000001"))
bcz = DirichletBC(V, f, "on_boundary")
r = 1.0e4
# define test and trial functions, and function that is updated
uold = TrialFunction(V)
v = TestFunction(V)
w = Function(V)
asf = inner(grad(uold), grad(v))*dx + r*div(uold)*div(v)*dx 
bs = -div(w)*div(v)*dx
uold = Function(V)
ust = Function(V)
pdes = LinearVariationalProblem(asf, - bs, uold, bc)
solvers = LinearVariationalSolver(pdes)
# Scott-Vogelius iterated penalty method
iters = 0; max_iters = 15; div_u_norm = 1
while iters < max_iters and div_u_norm > 1e-10:
# solve and update w
    solvers.solve()
    w.vector().axpy(-r, uold.vector())
# find the L^2 norm of div(u) to check stopping condition
    div_u_norm = sqrt(assemble(div(uold)*div(uold)*dx(mesh)))
#       print "   IPM iter_no=",iters,"div_u_norm="," %.2e"%div_u_norm
    iters += 1
print "Stokes solver  IPM iter_no=",iters,"div_u_norm="," %.2e"%div_u_norm
#plot(uold[0], interactive=True)
uold.set_allow_extrapolation(True)
seeu=interpolate(uold,W)
#plot(seeu[0], interactive=True)
ust.vector().axpy(1.0, uold.vector())
# set the variational problem
#for reno in [0.0001]:
#for reno in [30,50]:
for reno in [30,40,50,80]:
#for reno in [30,50,80,90]:
##for reno in [30,50,80,90,100]:
#for reno in [50,80,90,100,120,140,160,180,200]:
    kters = 0; max_kters = 9; unorm = 1
    while kters < max_kters and unorm > 1e-6:
        u = TrialFunction(V)
        v = TestFunction(V)
        w = Function(V)
#   uold = Function(V)
        a = inner(grad(u), grad(v))*dx + r*div(u)*div(v)*dx \
          +reno*inner(grad(uold)*u,v)*dx+reno*inner(grad(u)*uold,v)*dx 
        b = -div(w)*div(v)*dx
        F = inner(grad(uold), grad(v))*dx+reno*inner(grad(uold)*uold,v)*dx
        u = Function(V)
        pde = LinearVariationalProblem(a, F - b, u, bcz)
        solver = LinearVariationalSolver(pde)
# Scott-Vogelius iterated penalty method
        iters = 0; max_iters = 11; div_u_norm = 1
        while iters < max_iters and div_u_norm > 1e-10:
    # solve and update w
            solver.solve()
            w.vector().axpy(-r, u.vector())
        # find the L^2 norm of div(u) to check stopping condition
            div_u_norm = sqrt(assemble(div(u)*div(u)*dx(mesh)))
    #       print "   IPM iter_no=",iters,"div_u_norm="," %.2e"%div_u_norm
            iters += 1
#       print "   IPM iter_no=",iters,"div_u_norm="," %.2e"%div_u_norm
        kters += 1
        uold.vector().axpy(-1.0, u.vector())
    #   uold=uold-u
        unorm=norm(u,norm_type='H1')
        uoldnorm=norm(uold,norm_type='H1')
        div_u_norm = sqrt(assemble(div(uold)*div(uold)*dx(mesh)))
    print "  Re=",reno,"Newton iter_no=",kters,"Delta_u_norm=","%.1e"%unorm, \
        "u_norm=","%.1e"%uoldnorm,"div_u_norm=","%.1e"%div_u_norm
#plot(uold[1], interactive=True)
uold.vector().axpy(-1.0, ust.vector())
uold.set_allow_extrapolation(True)
seeu=interpolate(uold,W)
plot(seeu[0], interactive=True,scalarbar=False,window_width=1200)