"""This solves Poisson's equation

    - div grad u(x, y) = f(x, y) ~ delta(x, y)

on the unit square with source f given by

    f(x, y) = amp^2*exp(-((x - 0.50001)^2 + (y - 0.50002)^2)/(2c^2))

and boundary conditions given by

    u(x, y) = (1/2\pi)*log(sqrt((x - 0.50001)^2+(y - 0.50002)^2))        on the boundary

The integral of f is 1 iff  c=1/(amp\sqrt{2\pi}) so that 
2c^2=2/(amp^2 2 \pi)=1/(amp^2 \pi). Thus 1/(2c^2)=\pi amp^2
    f(x, y) = amps*exp(-((x - 0.50001)^2 + (y - 0.50002)^2)*(pi*amps))
Note that
    u(x, y) = (1/2\pi)*(1/2)*log(((x - 0.50001)^2+(y - 0.50002)^2))        
"""
from dolfin import *
import sys,math

meshsize=int(sys.argv[1])
pdeg=int(sys.argv[2])
amps=float(sys.argv[3])
ba=amps*math.pi
dfac=1/(4*math.pi)

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define boundary condition
u0 = Expression("-d*log(pow(x[0] - 0.50001,2)+pow(x[1] - 0.50002,2))",d=dfac,degree=pdeg)
onec = Expression("1.0",degree=pdeg)
bc = DirichletBC(V, u0, DomainBoundary())

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("aa*exp(-(pow(x[0]-0.50001,2) + pow(x[1]-0.50002,2))*b)",aa=amps,b=ba,degree=pdeg)
a = inner(grad(u), grad(v))*dx
L = f*v*dx

# Compute solution
u = Function(V)
solve(a == L, u, bc)
uone=project(onec,V)
uzed=interpolate(u0,V)
fo=interpolate(f,V)
efo= 1-assemble(onec*fo*dx)
print " ",pdeg," ",meshsize," %.2e"%amps, \
      " %.2e"%errornorm(u0,u,norm_type='l2', degree_rise=0)," %.2e"%efo
plot(u, interactive=True)