"""This demo program solves Poisson's equation

    - a div grad u(x, y) + u(x,y) = f

on the unit square with Dirichlet boundary conditions given by u=0 
on the boundary of the square, whose solution is

    u(x, y) ~  f  in the interior as a --> 0 

"""

from dolfin import *
import sys,math

meshsize=2
pdeg=1
acoef=0.001
mytol=0.001

# Create mesh and define function space
mesh = UnitSquareMesh(meshsize, meshsize)
V = FunctionSpace(mesh, "Lagrange", pdeg)

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, "on_boundary")

# Define variational problem
u = Function(V)
v = TestFunction(V)
f = Expression("1.0")
F= (acoef*inner(grad(u), grad(v))+u*v)*dx - f*v*dx 
J = u*u*dx

# Compute solution
solve(F == 0, u, bc, tol=mytol, M=J)

#plot(u, interactive=True)
plot(u.leaf_node(), interactive=True)